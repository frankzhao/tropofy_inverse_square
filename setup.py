from setuptools import setup, find_packages

requires = [
    'tropofy',
    'numpy',
    'matplotlib <= 1.5.1',
]

setup(
    name='tropofy-inverse_square',
    version='1.0',
    description='Inverse square law plotter',
    author='Frank Zhao',
    url='https://frankzhao.com.au',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
)
