Inverse square
==============

One of the first problems I like to solve when starting with a new analysis framework (e.g Mathematica, Matlab)
is to calculate the superposition of a bunch of point sources.

Plot point sources that obey the inverse square law. For a point source with intensity A1 at a distance D1,
the intensity A2 at a distance D2 is given by:

```
A1/A2 = (D1/D2)^2
```

This app calculates and plots the intensity of arbitrary source functions in 2D space. A plot is generated composing
of the superposition of the sources.

For example, the following is a plot of the intensity interaction of two sinusoidal sources and one sqrt source
centered at three different positions.

![Example output plot](https://gitlab.com/frankzhao/tropofy_inverse_square/raw/777bb3083b93084a3224495d10c66f71e5dc7a80/example_output.png "Example plot")

