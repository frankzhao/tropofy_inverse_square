import StringIO
import logging

import matplotlib.pyplot as plt
import numpy as np
import pkg_resources
import pylab as pl
from matplotlib.colors import Normalize
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Float, String
from tropofy.app import AppWithDataSets, Parameter, Step, StepGroup, ParameterGroup
from tropofy.database.tropofy_orm import DataSetMixin
from tropofy.file_io import read_write_xl
from tropofy.widgets import StaticImage, ExecuteFunction, ParameterForm, SimpleGrid

# noinspection PyUnresolvedReferences
from mpl_toolkits.mplot3d import Axes3D

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
SITE_ROOT = '/usr/share/nginx/html'     # Directory to write the plot output to be served


class IntensityFunction(DataSetMixin):
    intensity = Column(Float, nullable=False, default=10)
    distance = Column(Float, nullable=False, default=1)
    frequency = Column(Float, nullable=False, default=1)
    x_pos = Column(Float, nullable=False, default=0)
    y_pos = Column(Float, nullable=False, default=0)
    function = Column(String, nullable=False, default='sin')


class OutputPlot(StaticImage):
    def get_file_path(self, app_session):
        return 'http://localhost/{}'.format('output.png')


def validate_positive(value):
    return True if value > 0 else "Value must be > 0."


def load_data(app_session):
    read_write_xl.ExcelReader.load_data_from_excel_file_on_disk(
        app_session, pkg_resources.resource_filename(
            'inverse_square', 'sample_data.xlsx'
        )
    )


class PlotConfiguration(ParameterGroup):
    MESH_SIZE = Parameter(name='mesh_size', label='Mesh Size', default=0.25, allowed_type=float,
                          validator=validate_positive)
    X_MIN = Parameter(name='x_min', label='Min X', default=-4, allowed_type=float)
    X_MAX = Parameter(name='x_max', label='Max X', default=4, allowed_type=float)
    Y_MIN = Parameter(name='y_min', label='Min Y', default=-4, allowed_type=float)
    Y_MAX = Parameter(name='y_max', label='Max Y', default=4, allowed_type=float)


class MyApp(AppWithDataSets):
    def get_name(self):
        return "Generate a plot of inverse square law decay functions from point sources."

    def get_gui(self):
        return [
            StepGroup(
                name='Input',
                steps=[
                    Step(name='Function', widgets=[SimpleGrid(IntensityFunction)]),
                    Step(name='Parameters', widgets=[{"widget": ParameterForm(), "cols": 6}]),
                    Step(name='Create Plot', widgets=[CreatePlot()]),
                ]
            ),
            StepGroup(name='Output', steps=[Step(name='Output', widgets=[OutputPlot()])])
        ]

    def get_parameters(self):
        return PlotConfiguration.get_params()

    def get_examples(self):
        return {"Sample": load_data}

    def get_home_page_content(self):
        return {
            'content_app_name_header': '''
            <div>
            <span style="vertical-align: middle;">
              Calculate the superposition of inverse square law decaying source functions
            </span>
            </div>'''
        }


class CreatePlot(ExecuteFunction):
    def get_button_text(self, app_session):
        return "Create plot"

    @staticmethod
    def intensity_function(x, gain=1, frequency=1, phase=0, function='sin'):
        """
        :param x: points
        :type x: np.ndarray
        :param gain: Amplitude of the function
        :type gain: float
        :param phase: Phase shift in radians
        :type phase: float
        :param frequency: Function frequency
        :type frequency: float
        :param function: Perturbation function, numpy attribute
        :type function: str
        :return: np.ndarray
        :rtype: np.ndarray
        """
        return gain * getattr(np, function)(2 * np.pi * frequency * x + phase)

    def execute_function(self, app_session):
        logger.setLevel(logging.DEBUG)

        MESH_SIZE = app_session.data_set.get_param(PlotConfiguration.MESH_SIZE.name)

        max_x_shift = max([x[0] for x in app_session.data_set.query(IntensityFunction.x_pos).all()])
        min_x_shift = min([x[0] for x in app_session.data_set.query(IntensityFunction.x_pos).all()])
        max_y_shift = max([x[0] for x in app_session.data_set.query(IntensityFunction.y_pos).all()])
        min_y_shift = min([x[0] for x in app_session.data_set.query(IntensityFunction.y_pos).all()])

        logger.debug('max_x_shift=%s, min_x_shift=%s, max_y_shift=%s, min_y_shift=%s,' %
                     (max_x_shift, min_x_shift, max_y_shift, min_y_shift))

        X = np.arange(
            app_session.data_set.get_param(PlotConfiguration.X_MIN.name) - min_x_shift,
            app_session.data_set.get_param(PlotConfiguration.X_MAX.name) + max_x_shift,
            MESH_SIZE
        )
        Y = np.arange(
            app_session.data_set.get_param(PlotConfiguration.Y_MIN.name) - min_y_shift,
            app_session.data_set.get_param(PlotConfiguration.Y_MAX.name) + max_y_shift,
            MESH_SIZE
        )

        x_mesh, y_mesh = np.meshgrid(X, Y)

        Z = None
        for fn in app_session.data_set.query(IntensityFunction).all():  # type: IntensityFunction
            logger.debug('intensity=%s, distance=%s, frequency=%s, x_pos=%s, y_pos=%s' %
                         (fn.intensity, fn.distance, fn.frequency, fn.x_pos, fn.y_pos))
            with np.errstate(divide='ignore', invalid='ignore'):
                logger.debug('Calculating perturbation')

                # Generate the distance grid
                distance = np.sqrt(
                    np.power(x_mesh + fn.x_pos, 2) + np.power(y_mesh + fn.y_pos, 2)
                )

                # Apply specified function perturbation
                perturb = self.intensity_function(
                    distance,
                    frequency=fn.frequency,
                    gain=fn.intensity,
                    function=fn.function
                )

                # Apply inverse square law
                logger.debug('Calculating result')
                h = perturb * (np.power(np.true_divide(fn.distance, distance), 2))
                h[h == np.inf] = 0
                h = np.nan_to_num(h)
                h[h > fn.intensity] = fn.intensity
                h[h < -fn.intensity] = -fn.intensity

                # Superpose
                Z = h if Z is None else Z + h

        if Z is None:
            app_session.task_manager.send_progress_message('Failed: No points generated')
            raise ValueError('Calculation failed!')

        # Create the plot
        fig = plt.figure()
        fig.set_figheight(10)

        # 3D surface plot
        ax = fig.add_subplot(211, projection='3d')
        ax.set_title('Inverse square law on a plane')
        norm = Normalize(vmin=np.min(Z), vmax=np.max(Z))
        ax.plot_surface(x_mesh, y_mesh, Z, rstride=1, cstride=1, cmap=pl.cm.inferno, norm=norm, linewidth=.2)
        ax.view_init(elev=45, azim=25)

        # 2D contour plot
        ax = fig.add_subplot(212)
        cf = ax.contourf(x_mesh, y_mesh, Z, zdir='z', cmap=pl.cm.inferno)
        fig.colorbar(cf)

        # Save the output
        img_data = StringIO.StringIO()
        plt.savefig(img_data, format="png", bbox_inches=0)
        img_data.seek(0)
        output = img_data.buf

        with open('{}/output.png'.format(SITE_ROOT), 'w') as f:
            f.write(output)
        app_session.task_manager.send_progress_message("Plot created.")
